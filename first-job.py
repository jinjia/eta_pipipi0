j = Job(name='First ganga job')
myApp = GaudiExec()
myApp.directory = "/afs/ihep.ac.cn/users/z/zhaojinjia/lamda/ntupleProd/DaVinciDev_v45r8"
j.application = myApp
j.application.options = ['/afs/ihep.ac.cn/users/z/zhaojinjia/lamda/ntupleProd/eta_3pi.py']
j.application.platform = 'x86_64_v2-centos7-gcc10-opt'
bkPath ='/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST'
#bkPath ='/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/DIMUON.DST'
data  = BKQuery(bkPath, dqflag=['OK']).getDataset()
j.inputdata = data
j.backend = Dirac()
j.splitter = SplitByFiles(filesPerJob=20,maxFiles= -1)
j.outputfiles = [LocalFile('Tuple_data1_X.root')]
j.submit()
