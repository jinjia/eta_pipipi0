############# Do not make any changes to this file!!!############
from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto
data_type = '2016'
doMC = False
type_ = "90000000"
outTag = ''			
#outTag = '/afs/ihep.ac.cn/users/z/zhaojinjia/5T_space/'			
stream_loc = 'Dimuion'		
line = 'FullDSTDiMuonJpsi2MuMuDetachedLine'	
def inspectA(x):		#
    print (x)
    if 'Members' in dir(x) and x.Members:
        for y in x.Members:
            inspectA(y)

mtl= ["L0MuonDecision",
      "L0DiMuonDecision",
      "Hlt1DiMuonHighMassDecision",
      "Hlt1SingleMuonNoIPDecision",
      "Hlt1TrackAllL0Decision",
      "Hlt1TrackMuonDecision",
      "Hlt1GlobalDecision",
      "Hlt2DiMuonDecision",
      "Hlt2DiMuonJPsiDecision",
      "Hlt2DiMuonPsi2SDecision",
      "Hlt2DiMuonBDecision",
      "Hlt2DiMuonDetachedDecision",
      "Hlt2DiMuonDetachedJPsiDecision",
      "Hlt2DiMuonDetachedPsi2SDecision",
      "Hlt2SingleMuonDecision",
	]


# Unit
SeqPhys = GaudiSequencer("SeqPhys")

###  add standard options
importOptions("$STDOPTS/PreloadUnits.opts")

from PhysSelPython.Wrappers import DataOnDemand, Selection, SelectionSequence, AutomaticData
#hohdwo;h

XX2 = CombineParticles ("XX2")
XX2.DecayDescriptor = " eta -> pi+ pi- pi0  "
XX2.Inputs = ['Phys/StdLooseResolvedPi0/Particles','Phys/StdLoosePions/Particles']
XX2.CombinationCut = "((AM > 400.*MeV) & (AM < 1200.*MeV))"
XX2.MotherCut = "(VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (PT > 1000.*MeV) & (BPVVDZ>0) & (BPVVDCHI2>25)"
XX2.DaughtersCuts = {
'pi0' : '(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05)',
'pi+' : '(PT > 150.*MeV) & (TRCHI2DOF < 4.0)',
'pi-' : '(PT > 150.*MeV) & (TRCHI2DOF < 4.0)'
}


#### Require to be refitted to primary vertex?
XX2.ReFitPVs = True

## tools used to dump the infomation to the ntuple
tl= [ "TupleToolKinematic", 
      "TupleToolPid",
      "TupleToolTrackInfo",
      "TupleToolPrimaries",
      "TupleToolEventInfo",
      "TupleToolTrackInfo",
      "TupleToolRecoStats",
      "TupleToolGeometry", 
      "TupleToolTISTOS",
      "TupleToolMCTruth",
      "TupleToolMCBackgroundInfo"]


#### create ntuple
tuple0 = DecayTreeTuple("tuple0")
tuple0.Inputs = ['Phys/XX2/Particles']
#tuple0.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream_loc, line)]
tuple0.Decay = " eta -> ^pi+ ^pi- ^(pi0-> ^gamma ^gamma) "
tuple0.Branches = {
        "piplus"        :   " eta -> ^pi+ pi- (pi0-> gamma gamma) ",
        "piminus"       :   " eta -> pi+ ^pi- (pi0-> gamma gamma) ",
        "pi0"           :   " eta -> pi+ pi- ^(pi0-> gamma gamma) ",
        "gamma1"         :   " eta -> pi+ pi- (pi0-> ^gamma gamma) ",
        "gamma2"         :   " eta -> pi+ pi- (pi0-> gamma ^gamma) ",
        "XX"            :   " eta -> pi+ pi- (pi0-> gamma gamma) "
   }
tuple0.ReFitPVs = True

from Configurables import LoKi__Hybrid__TupleTool
tuple0.ToolList = tl

# extra variables for XX
LoKi_XX=LoKi__Hybrid__TupleTool("LoKi_XX")
LoKi_XX.Variables = {
    'ETA': 'ETA',
    'Y'  : 'Y',
    'LV01'  : 'LV01',
    'LV02'  : 'LV02',
    }

LoKi_gamma=LoKi__Hybrid__TupleTool("LoKi_gamma")
LoKi_gamma.Variables = {
    "ETA": "ETA",
    "Y"  : "Y",
    "VertexCHI2_dof" : "VFASPF(VCHI2/VDOF)",
    "VertexCHI2" : "VFASPF(VCHI2)",
    "BPVVDCHI2" : "BPVVDCHI2",
    "VFASPFVZ" : "VFASPF(VZ)",
    'CL': 'CL',
   # 'IsPhoton' : 'ProtoInfo(IsPhoton,0,-1000)'
    }
tuple0.addTool(TupleToolDecay, name='gamma')
tuple0.gamma.addTool(LoKi_gamma)
tuple0.gamma.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_gamma"]

tuple0.addTool(TupleToolDecay, name='XX')
from Configurables import TupleToolDecayTreeFitter
tuple0.XX.addTool(LoKi_XX)
tuple0.XX.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_XX"]

MassFit = TupleToolDecayTreeFitter("MassFit")
MassFit.Verbose = True
MassFit.constrainToOriginVertex = True
MassFit.daughtersToConstrain = ["pi0"]

tuple0.XX.addTool(MassFit)
tuple0.XX.ToolList += ["TupleToolDecayTreeFitter/MassFit"]

# event tuple
from Configurables import LoKi__Hybrid__EvtTupleTool
LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
LoKi_EvtTuple.VOID_Variables = { 
	"LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
	"LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
	"LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
	"LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
	"LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
	"LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
	"LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
	}   

tuple0.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
tuple0.addTool(LoKi_EvtTuple)

## primary vertex selection
from Configurables import CheckPV
checkpv = CheckPV()

# #### create ntuple
from Configurables import DaVinci
#SeqPhys.Members += [checkpv,scaler,tuple0]

########################################################################
from Configurables import TrackScaleState as SCALER
scaler = SCALER( 'Scaler' )
SeqPhys.Members += [checkpv,scaler,XX2,tuple0]
#DaVinci().UserAlgorithms  = [ scaler ]
DaVinci().UserAlgorithms += [SeqPhys]       # two trees, for reco and gene information 

    
#DaVinci().appendToMainSequence([checkpv])
DaVinci().DataType = data_type
DaVinci().Lumi = False

DaVinci().EvtMax = -1                      # Number of events
DaVinci().InputType = 'DST'
DaVinci().PrintFreq = 1000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().HistogramFile = outTag+"DVHistos_data1_X.root"      # Histogram file
DaVinci().TupleFile = outTag+"Tuple_data1_X.root"             # Ntuple1



### no need to give condDB and dddB
### for data, it automatically use the latest and best: https://lhcb.github.io/starterkit-lessons/first-analysis-steps/minimal-dv-job.html
### for MC, we use Ganga j.application.autoDBtags = True option 
### for testing.... we don't care?
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('Stripping"+line+"Decision')"
)
DaVinci().EventPreFilters = fltrs.filters('Filters')

from GaudiConf import IOHelper

# Use the local input data
IOHelper().inputFiles([
#    '/afs/ihep.ac.cn/users/z/zhaojinjia/5T_space/00103400_00000048_1.dimuon.dst'
    #'./00147466_00000230_5.AllStreams.dst'
    #'./00091107_00002312_5.AllStreams.dst'
    #'./00102452_00011313_1.dimuon.dst'
    #'./00147572_00000266_6.AllStreams.dst'
], clear=True)
