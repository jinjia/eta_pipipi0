############# Do not make any changes to this file!!!############
from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto
data_type = '2016'
doMC = False
type_ = "90000000"
outTag = ''			
stream_loc = 'DiMuon'		
line = 'FullDSTDiMuonJpsi2MuMuDetached'	
#rootInTes = "/Event/Bhadron/"
#from PhysConf.MicroDST import uDstConf
#uDstConf ( rootInTes )

def inspectA(x):		#
    print (x)
    if 'Members' in dir(x) and x.Members:
        for y in x.Members:
            inspectA(y)

# Unit
SeqPhys = GaudiSequencer("SeqPhys")
SeqPhys.RootInTES = rootInTes
###  add standard options
importOptions("$STDOPTS/PreloadUnits.opts")

from PhysSelPython.Wrappers import DataOnDemand, Selection, SelectionSequence, AutomaticData

#line_Location = "/Phys/Eta3PiforB2XEta/Particles"
#my_line = AutomaticData(Location = line_Location )

#f2_Location = '/Phys/FourTracks_NBodyDecay_ForRadiativeBBeauty2XGamma/Particles'
#f2_Location = '/Phys/StdLoosePions/Particles'
#my_f2 = AutomaticData(Location = f2_Location )
#gamma_Location = '/Phys/StdLooseResolvedPi0/Particles'
#my_gamma = AutomaticData(Location = gamma_Location )

XX2 = CombineParticles ("XX2")
XX2.DecayDescriptor = "eta -> pi+ pi- pi0"
XX2.Inputs = ['Phys/StdLooseResolvedPi0/Particles','Phys/StdLoosePions/Particles']
XX2.CombinationCut = "((AM > 200.*MeV) & (AM < 2200.*MeV))"
XX2.MotherCut = "(VFASPF(VCHI2)<9) & (BPVDIRA>0.95)"
XX2.DaughtersCuts = {
'pi0' : '(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05)'

}
#XX2.Inputs = ['/Event/Leptonic/Phys/FourTracks_NBodyDecay_ForRadiativeBBeauty2XGamma/Particles','/Event/Leptonic/Phys/StdLooseAllPhotons/Particles']
#XX2.Inputs = ['/Phys/FourTracks_NBodyDecay_ForRadiativeBBeauty2XGamma/Particles','/Phys/StdLooseAllPhotons/Particles']
#XX2.Inputs = [my_f2,my_gamma]

#XX2.RootInTES = rootInTes
#XX2.CombinationCut = "((AM > 400.*MeV) & (AM < 1000.*MeV))"
#XX2.MotherCut = "(PT>2000*MeV)&(VFASPF(VCHI2/VDOF)<10.0)"
#XX2.DaughtersCuts = {
 #   'pi+' : '(PT>300.0*MeV)&(TRCHI2DOF<4.0)&(TRGHOSTPROB<0.5)&(PROBNNpi>0.1)',
  #  'pi-' : '(PT>300.0*MeV)&(TRCHI2DOF<4.0)&(TRGHOSTPROB<0.5)&(PROBNNpi>0.1)'
#}

#### Require to be refitted to primary vertex?
XX2.ReFitPVs = True

## tools used to dump the infomation to the ntuple
tl= [ "TupleToolKinematic", 
      "TupleToolPid",
      "TupleToolTrackInfo",
      "TupleToolPrimaries",
      "TupleToolEventInfo",
      "TupleToolTrackInfo",
      "TupleToolRecoStats",
      "TupleToolGeometry", 
      "TupleToolTISTOS",
      "TupleToolMCTruth",
      "TupleToolMCBackgroundInfo"]

#### create ntuple
tuple0 = DecayTreeTuple("tuple0")
tuple0.Inputs = ['Phys/XX2/Particles']
#tuple0.Inputs = ["/Event/{0}/Phys/{1}/Particles".format('Bhadron','Eta3PiforB2XEta')]
#tuple0.Inputs = ["/Event/Leptonic/Phys/Beauty2XGamma4pi_Line/Particles"]
#tuple0.RootInTES = rootInTes
#tuple0.Inputs = [line_Location]
#tuple0.RootInTES = rootInTes
tuple0.Decay = " eta -> ^pi+ ^pi- ^(pi0-> ^gamma ^gamma) "
tuple0.Branches = {
        
        "piplus"    :    " eta -> ^pi+ pi- (pi0-> gamma gamma) ",
        "piminus"   :    " eta -> pi+ ^pi- (pi0-> gamma gamma) ",
        "pi0"       :    " eta -> pi+ pi- ^(pi0-> gamma gamma) ",
        "gamma1"    :    " eta -> pi+ pi- (pi0-> ^gamma gamma) ",
        "gamma2"    :    " eta -> pi+ pi- (pi0-> gamma ^gamma) ",
        "XX"        :    " eta -> pi+ pi- (pi0-> gamma gamma) ",
}

tuple0.ReFitPVs = True

from Configurables import LoKi__Hybrid__TupleTool
tuple0.ToolList = tl

# extra variables for XX
LoKi_XX=LoKi__Hybrid__TupleTool("LoKi_XX")
LoKi_XX.Variables = {
    'ETA': 'ETA',
    'Y'  : 'Y',
    'LV01'  : 'LV01',
    'LV02'  : 'LV02',
    }

LoKi_gamma=LoKi__Hybrid__TupleTool("LoKi_gamma")
LoKi_gamma.Variables = {
    "ETA": "ETA",
    "Y"  : "Y",
    "VertexCHI2_dof" : "VFASPF(VCHI2/VDOF)",
    "VertexCHI2" : "VFASPF(VCHI2)",
    "BPVVDCHI2" : "BPVVDCHI2",
    "VFASPFVZ" : "VFASPF(VZ)",
    'CL': 'CL',
   # 'IsPhoton' : 'ProtoInfo(IsPhoton,0,-1000)'
    }
tuple0.addTool(TupleToolDecay, name='gamma')
tuple0.gamma.addTool(LoKi_gamma)
tuple0.gamma.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_gamma"]

tuple0.addTool(TupleToolDecay, name='XX')
from Configurables import TupleToolDecayTreeFitter
tuple0.XX.addTool(LoKi_XX)
tuple0.XX.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_XX"]


from Configurables import LoKi__Hybrid__EvtTupleTool
LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
LoKi_EvtTuple.VOID_Variables = { 
	"LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
	"LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
	"LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
	"LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
	"LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
	"LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
	"LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
	}   

tuple0.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
tuple0.addTool(LoKi_EvtTuple)


## primary vertex selection
from Configurables import CheckPV
checkpv = CheckPV()

# #### create ntuple
from Configurables import DaVinci
SeqPhys.Members += [tuple0]

########################################################################
DaVinci().RootInTES = rootInTes
from Configurables import TrackScaleState as SCALER
scaler = SCALER( 'Scaler' )
DaVinci().UserAlgorithms  = [ scaler ]
DaVinci().UserAlgorithms += [SeqPhys]

DaVinci().appendToMainSequence([checkpv])
DaVinci().DataType = data_type
DaVinci().Lumi = False

DaVinci().EvtMax = -1                      # Number of events
DaVinci().InputType = 'MDST'
DaVinci().PrintFreq = 1000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().HistogramFile = outTag+"DVHistos_data1_X.root"      # Histogram file
#DaVinci().HistogramFile = "/publicfs/lhcb/user/zhaojinjia/"+outTag+"DVHistos_data1_X.root"      # Histogram file
#DaVinci().TupleFile ="/publicfs/lhcb/user/zhaojinjia/"+outTag+"Tuple_data1_X.root"             # Ntuple1
DaVinci().TupleFile = outTag+"Tuple_data1_X.root"             # Ntuple1

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('Stripping"+line+"Decision')"
)
DaVinci().EventPreFilters = fltrs.filters('Filters')

from GaudiConf import IOHelper

# Use the local input data
IOHelper().inputFiles([
    #'/publicfs/lhcb/user/zhaojinjia/00103400_00001653_1.leptonic.mdst'
    '/afs/ihep.ac.cn/users/z/zhaojinjia/5T_space/00103400_00000103_1.bhadron.mdst'
], clear=True)
